import styled from 'styled-components';

export default styled.h1`
  margin-left: 20px;

  font-size: 20px;
  font-weight: 900;
  text-transform: uppercase;
  color: #ffffff;

  opacity: 1;
`;
