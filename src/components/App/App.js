/* eslint-disable react/prop-types */
import React, { Fragment } from 'react';
import { Switch, Route } from 'react-router-dom';

import './globalStyles';

import Preloader from '../Preloader';
import Map from '../Map';
import Menu from '../Menu/Menu';
import Chart from '../Chart';
import Aside from '../Aside';
import CampFilters from '../CampFilters';
import Search from '../Search';
import Chronology from '../Chronology';
import About from '../About';
import CampCard from '../CampCard';
import Gallery from '../PhotoGallery';

const App = () => {
  document.title = 'В центре него бог';

  return (
    <Fragment>
      <Preloader />
      <Map />
      <Menu />
      <Aside />
      <Switch>
        {/* <Route path='/search' component={Search} /> */}
        <Route path='/about' component={About} />
        <Route path='/gallery' component={Gallery} />
        <Route
          path='/place:id'
          render={props => <CampCard key={props.match.params.id} {...props} />}
        />
      </Switch>
    </Fragment>
  );
};

export default App;
