import styled from 'styled-components';

export default styled.input`
  width: 100%;
  padding-bottom: 50px;
  border: none;
  border-bottom: solid 1px rgba(255, 255, 255, 0.1);
  margin-bottom: 20px;

  outline: none;

  font-size: 20px;
  font-weight: 900;
  color: rgba(255, 255, 255, 1);

  opacity: 0.7;

  background-color: transparent;
`;
