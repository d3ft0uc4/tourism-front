import styled from 'styled-components';

export default styled.div`
  margin-bottom: 10px;

  font-size: 16px;
  color: rgba(255, 255, 255, 1);
`;
