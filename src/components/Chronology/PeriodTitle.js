import styled from 'styled-components';

export default styled.div`
  margin-bottom: 10px;

  font-size: 20px;
  font-weight: 900;
  color: rgba(255,255,255);
`;
