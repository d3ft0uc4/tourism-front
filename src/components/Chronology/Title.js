import styled from 'styled-components';

export default styled.div`
  margin-bottom: 40px;

  font-size: 36px;
  font-weight: 900;
  color: rgba(255,255,255);
`;
