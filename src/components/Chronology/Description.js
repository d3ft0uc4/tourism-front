import styled from 'styled-components';

export default styled.div`
  margin-bottom: 40px;

  font-size: 16px;
  line-height: 1.56;
  color: rgba(255,255,255);
`;
