import styled from 'styled-components';

export default styled.div`
  position: absolute;
  left: -90px;

  font-size: 20px;
  color: rgba(255, 255, 255, 1);

  @media (max-width: 890px) {
    position: static;
  }
`;
