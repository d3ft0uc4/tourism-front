import React from 'react';
import PropTypes from 'prop-types';
import { t } from '../../intl/helper';

// ico
import cross from '../cross.svg';

// styled
import Container from './Container';
import CloseButton from './CloseButton';
import Button from './Button';
// import LangButton from './LangButton';

const Aside = props => {
  const {
    isMenuOpen,
    closeMenu,
    pushToAbout,
    pushToGallery
  } = props;

  return (
    <Container isMenuOpen={isMenuOpen} in={isMenuOpen}>
      <CloseButton onClick={closeMenu}>
        <img src={cross} alt='close' />
      </CloseButton>
      <div>
        <Button onClick={pushToAbout}>{t('aboutCard.heading')}</Button>
        <Button onClick={pushToGallery}>{t('galleryCard.heading')}</Button>
      </div>
    </Container>
  );
};

Aside.propTypes = {
  isMenuOpen: PropTypes.bool.isRequired,
  closeMenu: PropTypes.func.isRequired,
  openCampFilters: PropTypes.func.isRequired,
  pushToAbout: PropTypes.func.isRequired,
  pushToGallery: PropTypes.func.isRequired
};

export default Aside;
