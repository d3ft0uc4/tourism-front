import styled from 'styled-components';

export default styled.div`
  position: fixed;

  display: flex;
  width: 100%;
  height: 100vh;
  justify-content: center;
  align-items: center;

  background-color: #000000;

  z-index: 1;
`;
