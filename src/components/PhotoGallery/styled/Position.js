import styled from 'styled-components';

export default styled.div`
  margin-top: 20px;

  font-size: 16px;

  color: rgba(255, 255, 255, 0.6);
`;
