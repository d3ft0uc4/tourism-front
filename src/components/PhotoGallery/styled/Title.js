import styled from 'styled-components';

export default styled.h1`
  margin: 0 0 40px;

  font-size: 36px;
  font-weight: 900;
  color: rgba(255, 255, 255, 1);

  opacity: 1;
`;
