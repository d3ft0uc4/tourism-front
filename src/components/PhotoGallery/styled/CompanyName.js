import styled from 'styled-components';

import { mainFontColour } from '../../../config/styles';

export default styled.div`
  padding-bottom: 10px;
  border-bottom: 1px solid rgba(255, 255, 255, 0.2);
  margin-bottom: 20px;

  font-size: 16px;

  color: ${mainFontColour};
  opacity: 0.8;
`;
