import React, {useState, useCallback} from 'react';

import FullScreenCard from '../FullScreenCard/FullScreenCard';

import Title from './styled/Title';
import Description from './styled/Description';
import SubTitle from './styled/SubTitle';
import Gallery from 'react-photo-gallery';
import Carousel, {Modal, ModalGateway} from 'react-images';

import 'react-awesome-slider/dist/styles.css';
import 'react-awesome-slider/dist/custom-animations/cube-animation.css';


const photos = [
    {
        src: process.env.PUBLIC_URL + '/portfolio/ph/1.jpg',
        width: 3,
        height: 2
    },
    {
        src: process.env.PUBLIC_URL + '/portfolio/ph/2.jpg',
        width: 3,
        height: 2
    },
    {
        src: process.env.PUBLIC_URL + '/portfolio/ph/3.jpg',
        width: 3,
        height: 2
    },
    {
        src: process.env.PUBLIC_URL + '/portfolio/ph/4.jpg',
        width: 3,
        height: 2
    },
    {
        src: process.env.PUBLIC_URL + '/portfolio/ph/5.jpg',
        width: 3,
        height: 2
    },
    {
        src: process.env.PUBLIC_URL + '/portfolio/ph/6.jpg',
        width: 3,
        height: 2
    },
    {
        src: process.env.PUBLIC_URL + '/portfolio/ph/7.jpg',
        width: 3,
        height: 2
    },
    {
        src: process.env.PUBLIC_URL + '/portfolio/ph/8.jpg',
        width: 3,
        height: 2
    },
    // {
    //     //     src: process.env.PUBLIC_URL + '/portfolio/ph/9.jpg',
    //     //     width: 3,
    //     //     height: 2
    //     // },
    {
        src: process.env.PUBLIC_URL + '/portfolio/ph/10.jpg',
        width: 3,
        height: 2
    }
];

const photos2 = [
    {
        src: process.env.PUBLIC_URL + '/portfolio/ph2/1.jpg',
        width: 3,
        height: 4
    },
    {
        src: process.env.PUBLIC_URL + '/portfolio/ph2/2.jpg',
        width: 3,
        height: 4
    },
    {
        src: process.env.PUBLIC_URL + '/portfolio/ph2/3.jpg',
        width: 3,
        height: 4
    },
    {
        src: process.env.PUBLIC_URL + '/portfolio/ph2/4.jpg',
        width: 3,
        height: 4
    },
    {
        src: process.env.PUBLIC_URL + '/portfolio/ph2/5.jpg',
        width: 3,
        height: 4
    },
    {
        src: process.env.PUBLIC_URL + '/portfolio/ph2/6.jpg',
        width: 3,
        height: 4
    },
    {
        src: process.env.PUBLIC_URL + '/portfolio/ph2/7.jpg',
        width: 3,
        height: 4
    },
    {
        src: process.env.PUBLIC_URL + '/portfolio/ph2/8.jpg',
        width: 3,
        height: 4
    },
    {
        src: process.env.PUBLIC_URL + '/portfolio/ph2/9.jpg',
        width: 3,
        height: 4
    },
    {
        src: process.env.PUBLIC_URL + '/portfolio/ph2/10.jpg',
        width: 3,
        height: 4
    },
    {
        src: process.env.PUBLIC_URL + '/portfolio/ph2/11.jpg',
        width: 3,
        height: 4
    }
];

const photos3 = [
    {
        src: process.env.PUBLIC_URL + '/portfolio/ph3/1.jpg',
        width: 3,
        height: 4
    },
    {
        src: process.env.PUBLIC_URL + '/portfolio/ph3/2.jpg',
        width: 4,
        height: 3
    },
    {
        src: process.env.PUBLIC_URL + '/portfolio/ph3/3.jpg',
        width: 4,
        height: 3
    },
    {
        src: process.env.PUBLIC_URL + '/portfolio/ph3/4.jpg',
        width: 3,
        height: 4
    },
    {
        src: process.env.PUBLIC_URL + '/portfolio/ph3/5.jpg',
        width: 4,
        height: 3
    },
    {
        src: process.env.PUBLIC_URL + '/portfolio/ph3/6.jpg',
        width: 3,
        height: 4
    },
    // {
    //     src: process.env.PUBLIC_URL + '/portfolio/ph3/7.jpg',
    //     width: 3,
    //     height: 4
    // },
    {
        src: process.env.PUBLIC_URL + '/portfolio/ph3/8.jpg',
        width: 3,
        height: 4
    },
    {
        src: process.env.PUBLIC_URL + '/portfolio/ph3/9.jpg',
        width: 4,
        height: 3
    },
    {
        src: process.env.PUBLIC_URL + '/portfolio/ph3/10.jpg',
        width: 4,
        height: 3
    },
    {
        src: process.env.PUBLIC_URL + '/portfolio/ph3/11.jpg',
        width: 4,
        height: 3
    }
];

const GalleryPart = ({photos, direction}) => {
    const [currentImage, setCurrentImage] = useState(0);
    const [viewerIsOpen, setViewerIsOpen] = useState(false);

    const openLightbox = useCallback((event, {photo, index}) => {
        setCurrentImage(index);
        setViewerIsOpen(true);
    }, []);

    const closeLightbox = () => {
        setCurrentImage(0);
        setViewerIsOpen(false);
    };
    return (<div>
        <Gallery photos={photos} onClick={openLightbox} direction={direction}  />
        <ModalGateway>
            {viewerIsOpen ? (
                <Modal onClose={closeLightbox}>
                    <Carousel
                        currentIndex={currentImage}
                        views={photos.map(x => ({
                            ...x,
                            srcset: x.srcSet,
                            caption: x.title
                        }))}
                    />
                </Modal>
            ) : null}
        </ModalGateway>
    </div>)
};

const PhotoGallery = ({closeCard}) => {


    return <FullScreenCard onClick={closeCard}>
        <Title>Фотографии</Title>
        <SubTitle></SubTitle>
        <Description>
            <GalleryPart photos={photos} direction={"column"}/>
        </Description>
        <SubTitle></SubTitle>
        <Description>
            <GalleryPart photos={photos2}/>
        </Description>
        <SubTitle></SubTitle>
        <Description>
            <GalleryPart photos={photos3}/>
        </Description>
    </FullScreenCard>
};

export default PhotoGallery;
