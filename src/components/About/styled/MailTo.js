import styled from 'styled-components';

export default styled.a`
  display: block;
  padding-top: 11px;
  padding-bottom: 14px;
  border: 1px solid rgba(255, 255, 255, 0.5);
  margin: 40px 0;
  color: rgba(255, 255, 255, 1);
  text-align: center;
  text-decoration: none;
  transition: 0.5s;

  &:hover {
    background: #ffffff;
    color: #000000;
  }
`;
