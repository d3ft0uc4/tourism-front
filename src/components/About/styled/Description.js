import styled from 'styled-components';

export default styled.div`
  margin-bottom: 0px;

  line-height: 1.56;

  font-size: 16px;
  color: rgba(255, 255, 255, 1);
`;
