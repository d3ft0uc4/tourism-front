import styled from 'styled-components';

export default styled.footer`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 70px;
  @media (max-width: 414px) {
    flex-direction: column;
  }
`;
