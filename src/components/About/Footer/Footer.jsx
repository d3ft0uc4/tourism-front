import React from 'react';
import PropTypes from 'prop-types';

// links
import insta from '../../../images/insta.png'

// styled
import Container from './styled/Container';
import Link from './styled/Link';

const Footer = ({ locale }) => (
  <Container>
    <Link
      key={1}
      href='https://t.me/bognesmog'
      target='_blank'
      rel='noreferrer noopener'
    >
      <img width='64' height='64' src={insta} alt='Telegram' />
    </Link>
  </Container>
);

Footer.propTypes = {
  locale: PropTypes.oneOf(['ru', 'en', 'de']).isRequired
};

export default Footer;
