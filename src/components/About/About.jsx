import React from 'react';
import PropTypes from 'prop-types';

// content

// components
import FullScreenCard from '../FullScreenCard/FullScreenCard';
import Footer from './Footer';

// styled
import Title from './styled/Title';
import Description from './styled/Description';
import SubTitle from './styled/SubTitle';
import MailTo from './styled/MailTo';

const About = ({ locale, closeCard }) => (
  <FullScreenCard onClick={closeCard}>
    <Title>О проекте</Title>
      <SubTitle>Что здесь происходит?</SubTitle>
    <Description>
      <p>
          Это первая интерактивная карта мистических и таинственных объектов России.
          Темный туризм – это посещение особенных мест с нетипичными историями, легендами и случившимися катастрофами.
          Точки на карте подобраны с позиции ментального воздействия, что говорит о составлении особой психогеографической карты.
          Места обитания сектантов и эзотерических подпольных кружков, заброшенные церкви с сохранившимся внутренним убранством,
          пространство, охваченное массовым психозом: всё феноменальное и странное собрано здесь.
      </p>
    </Description>
    <SubTitle>О создателе</SubTitle>
    <Description>
      <p>
          Меня зовут Вероника, по образованию я историк.
          Поэтому для выбора мест на карту мне важно:
          <ul>
              <li>чтобы имеющиеся мифы вокруг объекта были подкреплены свидетельствами;</li>
              <li>
                  чтобы место существовало на данный момент, и его можно было посетить в оригинальной аутентичности.
              </li>
          </ul>


          Меня интересует надрыв, пограничность, размазанность, повторения, достижение
          клокочущего состояния и тревога как всеобъемлющее чувство.
          Я обожаю попадать в места, способные вызвать подобные ощущения.
          Уверена, что есть ещё множество людей,
          которые хотели бы иметь наглядное собрание таких колоритных мест на территории России.
      </p>
{/*      <p>*/}
{/*Если Вам есть, что предложить в качестве материала для проекта, пишите сюда:*/}
{/*        <MailTo href='mailto:darktourism@mail.ru'>darktourism@mail.ru</MailTo>*/}
{/*      </p>*/}

    </Description>
    <Footer locale={locale} />
  </FullScreenCard>
);

About.propTypes = {
  locale: PropTypes.PropTypes.oneOf(['ru', 'en', 'de']).isRequired,
  closeCard: PropTypes.func.isRequired
};

export default About;
