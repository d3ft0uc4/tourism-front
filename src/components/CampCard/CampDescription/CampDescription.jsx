/* eslint-disable react/no-array-index-key */
import React from 'react';
import PropTypes from 'prop-types';

import parseMarkup from '../../../utils/parseMarkup';

// ico
// import closeIcon from '../icons/btn-close.svg';

import Description from './Description';
// import PhotoGallery from './PhotoGallery/PhotoGallery';

// styled
import Container from './Container';
// import { CardButton } from '../StyledButtons';

const CampDescription = ({ markup }) => (
  <Container id='campDescription'>
    {parseMarkup(markup).map((elem, i) => {
      switch (elem.type) {
        case 'description': {
          return <Description key={i} md={elem.payload} isIncut={false} />;
        }
        case 'incut': {
          return <Description key={i} md={elem.payload} isIncut />;
        }
        default:
          return null;
      }
    })}
  </Container>
);

CampDescription.propTypes = {
  markup: PropTypes.string.isRequired
};

export default CampDescription;
