export default {
  locale: 'ru',
  messages: {
    'menu.title': 'В центре него бог',
    showAllButton: 'Весь период',
    'prisonCard.yearsOfOperation': 'Годы существования',
    'prisonCard.location': 'Местоположение',
    'prisonCard.prisonersByYears': 'Количество заключенных по годам',
    'prisonCard.production': 'Тип деятельности',
    'prisonCard.photo': 'Фото',
    'aboutCard.heading': 'О проекте',
    'galleryCard.heading': 'Фотографии',
    'aside.search': 'Поиск',
    'aside.campTypes': '',
    'aside.gulagChronology': '',
    'campFilters.title': '',
    prisoners: 'заключённых',
    dead: 'умерших',
    noData: 'нет данных'
  }
};
