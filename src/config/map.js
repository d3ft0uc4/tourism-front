/* eslint-disable max-len */
export default {
  accessToken:
    'pk.eyJ1IjoiZGVmdG91Y2giLCJhIjoiY2psd2Y1ajQ4MTBzNTNrbDg2cnMxNWx6cyJ9.ePmwSxGIaVOCwsb_I4yQKg',
  mapStyle: 'mapbox://styles/deftouch/ckic6s1hr1f6i19oa9k3q102t',
  maxZoom: 15,
  minZoom: 2,
  pitchWithRotate: false,
  dragRotate: false,
  latitude: 55.751244,
  longitude: 37.618423,
  // zoom: 10
};
