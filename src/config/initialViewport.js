const desctop = {
  latitude: 55.751244,
  longitude: 37.618423,
  zoom: 5
};
const mobile = {
  latitude: 55.751244,
  longitude: 37.618423,
  zoom: 5
};

export default (window.innerWidth > 719 ? desctop : mobile);